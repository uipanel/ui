<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Variations extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => TRUE,
            ),
            'title' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE,
            ),
            'value' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => TRUE,
            ),
            'productId' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'null' => TRUE,
            ),
        ));
    
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('variations');
        $this->db->query(add_foreign_key('variations', 'productId', 'products(id)', 'CASCADE', 'CASCADE'));
    }
    
    public function down()
    {
        $this->db->query(drop_foreign_key('variations', 'productId'));
        $this->dbforge->drop_table('files');
    }
    
}