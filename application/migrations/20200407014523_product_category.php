<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Product_category extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'auto_increment' => TRUE,
            ),
            'productId' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'null' => TRUE,
            ),
            'categoryId' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'null' => TRUE,
            ),
        ));
    
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('product_category');
        $this->db->query(add_foreign_key('product_category', 'productId', 'products(id)', 'CASCADE', 'CASCADE'));
        $this->db->query(add_foreign_key('product_category', 'categoryId', 'categories(id)', 'CASCADE', 'CASCADE'));
    }
    
    public function down()
    {
        $this->db->query(drop_foreign_key('product_category', 'productId'));
        $this->db->query(drop_foreign_key('product_category', 'categoryId'));
        $this->dbforge->drop_table('files');
    }
    
}