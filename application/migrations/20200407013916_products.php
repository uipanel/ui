<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Products extends CI_Migration {

public function up()
{
    $this->dbforge->add_field(array(
        'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE,
        ),
        'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
        ),
        'url' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
        ),
        'description' => array(
                'type' => 'TEXT',
                'null' => TRUE,
        ),
        'price' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE,
        ),
        'discountedPrice' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE,
        ),
        'isActive' => array(
                'type' => 'TINYINT',
                'null' => TRUE,
        ),
        'createdTime' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
        ),
        'updatedTime' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
        ),
        'categoryId' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
        ),
    ));

    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('products');
}

public function down()
{
    $this->dbforge->drop_table('products');
}

}