<?php

class Product_model extends CI_Model{
    public $table_name = 'products';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all(){
        return $this->db->get($this->table_name)->result();
    }

    public function insert_data($data, $vData, $pData, $iData) {

        for ($j = 1; $j <= 2; $j++) {

            for ($i = 1; $i <= 3; $i++) {

                for ($k = 0; $k < $iData['product_images_count']; $k++) {
                    $this->db->set('baslik', $data['baslik']);
                    $this->db->set('aciklama', $data['aciklama']);
                    $this->db->set('fiyat', $data['fiyat']);
                    $this->db->set('indirimsizFiyat', $data['indirimsizFiyat']);
                    $this->db->set('tip', $data['tip']);
                    $this->db->set('varyasyonAdi', $vData['varyasyonAdi'][$j]);
                    $this->db->set('varyasyonDegeri', $vData['varyasyonDegeri'][$j]);
                    $this->db->set('stok', $data['stok']);
                    $this->db->set('stokKodu', $data['stokKodu']);
                    $this->db->set('kategori', $data['kategori']);
                    $this->db->set('gorsel', $iData['gorsel'][$k]);
                    $this->db->set('nitelikAdi', $pData['nitelikAdi'][$i]);
                    $this->db->set('nitelikDegeri', $pData['nitelikDegeri'][$i]);
                    $this->db->insert('deneme');
                }
            
            }    

        }

    }
}