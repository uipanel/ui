<?php

class Dashboard extends CI_Controller {

    public $viewFolder = "";

    public function __construct() {
        parent::__construct();

        $this->viewFolder = "dashboard_view";
    }

	public function index() {
        $this->lang->load('dashboard','tr');
        
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
		
		$this->load->view("{$viewData->viewFolder}/index", $viewData);
    }

}
