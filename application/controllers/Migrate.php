<?php

class Migrate extends CI_Controller {

    // her fonksiyonda çalışacak construct
    public function __construct() {
        parent::__construct();

        // codeigniter izin komutu, ilk başta bunu kontrol ediyoruz
        if (!$this->input->is_cli_request()) {
            //hata yazdır
            show_error("You don\'t have permission for this action");

            return;
        }

        // izin varsa migration kütüphanesini yükle
        $this->load->library('migration');
    }

    public function version($version) {
        // migration değişkenine verison fonksiyonunu entegre ederek migrate kütüphanesi çağırıyoruz
        $migration = $this->migration->version($version);

        if (!$migration) {
            echo $this->migration->error_string();
        } else {
            echo 'Migration done' . PHP_EOL;
        }
        
    }

    // generate fonksiyonunu name değişkeniyle oluşturuyoruz
    public function generate($name = false) {

        // name boş-doluluk kontrolü
        if (!$name) {
            echo 'Please define migration name' . PHP_EOL;

            return;
        }

        // name karakter kontrolleri
        if (!preg_match('/^[a-z_]+$/i', $name)) {

            if (strlen($name) < 4) {
                echo 'Migration must be at least 4 characters long' . PHP_EOL;

                return;
            }

            echo "Wrong migration name, allowed characters a-z and _\nExample: first_migration" . PHP_EOL;

            return;
        }

        // dosya ismi oluşturma: tarih + isim + .php
        $filename = date('YmdHis') . '_' . $name . '.php';

        try {
            // migrations klasör yolu
            $folderPath = APPPATH . 'migrations';

            // eğer dizin yoksa oluşturmayı dene, yoksa hata bastır
            if (!is_dir($folderPath)) {

                try {
                    mkdir($folderPath);
                } catch (Exception $e) {
                    echo "Error:\n" . $e->getMessage() . PHP_EOL;

                    return;
                }

            }

            // migrations dosya yolu
            $filepath = APPPATH . 'migrations/' . $filename;

            // eğer dosya varsa
            if (file_exists($filepath)) {
                echo "File already exist:\n" . $filepath . PHP_EOL;

                return;
            }

            // data adında bir array oluşturduk ve className'ine dosya ismini baş harfi büyük olarak verdik
            $data['className'] = ucfirst($name);
            // template içine migration taslağını, arrayi ve true'yu verdik
            $template = $this->load->view('cli/migrations/migration_class_template', $data, TRUE);

            // dosyanın içine yazmayı dene yoksa hata yakala
            try {
                $file = fopen($filepath, "w"); // dosya aç
                // dosya içine <?php ve template içindekileri yaz
                $content = "<?php\n" . $template;

                fwrite($file, $content);
                fclose($file);
            } catch (Exception $e) {
                echo "Error:\n" . $e->getMessage() . PHP_EOL;
            }

            echo "Migration created successfully!\nLocation: " . $filepath . PHP_EOL;
        } catch (Exception $e) {
            echo "Can't create migration file!\nError: " . $e->getMessage() . PHP_EOL;
        }

    }
    
}