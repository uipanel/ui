<?php

class Products extends CI_Controller {

    public $viewFolder = "";

    public function __construct() {
        parent::__construct();

        $this->viewFolder = "products_view";
        $this->load->model("product_model");
    }

	public function index() {
        $this->lang->load('dashboard','tr');

        $viewData = new stdClass();
        $items = $this->product_model->get_all();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $items;

		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function add() {
        $this->lang->load('dashboard','tr');

        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function insert() {

        for ($i = 1; $i <= 2; $i++) {
            $vData['varyasyonAdi'][$i] = $this->input->post('product_variation_key_'.$i);
            $vData['varyasyonDegeri'][$i] = $this->input->post('product_variation_value_'.$i);
        }

        for ($j = 1; $j <= 3; $j++) {
            $pData['nitelikAdi'][$j] = $this->input->post('product_property_key_'.$j);
            $pData['nitelikDegeri'][$j] = $this->input->post('product_property_value_'.$j);
        }

        $product_images_count = count($_FILES['product_images']['name']);

        $iData['product_images_count'] = $product_images_count;

        for ($i = 0; $i < $product_images_count; $i++) {
            $_FILES['file']['name'] = $_FILES['product_images']['name'][$i]; 
            $_FILES['file']['type'] = $_FILES['product_images']['type'][$i]; 
            $_FILES['file']['tmp_name'] = $_FILES['product_images']['tmp_name'][$i]; 
            $_FILES['file']['error'] = $_FILES['product_images']['error'][$i]; 
            $_FILES['file']['size'] = $_FILES['product_images']['size'][$i];
            $config['upload_path'] = './uploads/'; 
            $config['allowed_types'] = 'jpg|jpeg|png|gif'; 
            
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')){ 
                $product_images = $this->upload->data();

                $iData['gorsel'][] = $product_images['file_name'];
            }
            
        }

        $data = array(
            'baslik' => $this->input->post('product_name'),
            'aciklama' => $this->input->post('products_description'),
            'fiyat' => $this->input->post('product_price'),
            'indirimsizFiyat' => $this->input->post('product_discount_price'),
            'tip' => $this->input->post('product_type'),
            'stok' => $product_stock = $this->input->post('product_stock'),
            'stokKodu' => $product_stock_code = $this->input->post('product_stock_code'),
            'kategori' => $this->input->post('product_category'),
        );

        $this->product_model->insert_data($data, $vData, $pData, $iData);

        redirect(base_url('products/add'));
    }

}
