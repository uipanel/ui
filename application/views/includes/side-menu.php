<div class="ecaps-sidemenu-area">
    <div class="ecaps-logo">
        <a href="index.html">
            <img class="desktop-logo" src="<?php echo base_url("assets") ?>\img\panel\logo.png" alt="Desktop Logo">
            <img class="small-logo" src="<?php echo base_url("assets") ?>\img\panel\small-logo.png" alt="Mobile Logo">
        </a>
    </div>
    <div class="ecaps-sidenav" id="ecapsSideNav">
        <div class="side-menu-area">
            <nav>
                <ul class="sidebar-menu" data-widget="tree">
                    <li><a href="<?php echo base_url("dashboard"); ?>"><i class="fa fa-columns"></i><span><?php echo $this->lang->line('main_page') ?></span></a></li>
                    <li><a href="<?php echo base_url("products"); ?>"><i class="fa fa-columns"></i><span><?php echo $this->lang->line('products') ?></span></a></li>
                    <li class="treeview">
                        <a href="javascript:void(0)">
                            <i class="fa fa-lira-sign"></i><span><?php echo $this->lang->line('turnover') ?></span><i class="fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="analytic-customer.html"><?php echo $this->lang->line('daily_turnover') ?></a></li>
                            <li><a href="analytic-report.html"><?php echo $this->lang->line('total_sales_per_day') ?></a></li>
                            <li><a href="analytic-report.html"><?php echo $this->lang->line('weekly_turnover') ?></a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="javascript:void(0)">
                            <i class="fa fa-shopping-cart"></i><span><?php echo $this->lang->line('orders') ?></span><i class="fa fa-angle-right"></i>
                        </a>
                        <!--<ul class="treeview-menu">
                            <li><a href="analytic-customer.html">Son Siparişler</a></li>
                        </ul>-->
                    </li>
                    <li class="treeview">
                        <a class="ui-slide">
                            <i class='bx bx-home-heart'></i><span><?php echo $this->lang->line('call_back') ?></span><i class="fa fa-angle-right"></i>
                        </a>
                        <!--<ul class="treeview-menu">
                            <li><a href="analytic-customer.html">Son Siparişler</a></li>
                        </ul>-->
                    </li>
                    <li class="treeview">
                        <a href="javascript:void(0)">
                            <i class='bx bx-home-heart'></i><span><?php echo $this->lang->line('support') ?></span><i class="fa fa-angle-right"></i>
                        </a>
                        <!--<ul class="treeview-menu">
                            <li><a href="analytic-customer.html">Son Siparişler</a></li>
                        </ul>-->
                    </li>
                    <li class="treeview">
                        <a href="javascript:void(0)">
                            <i class='bx bx-home-heart'></i><span><?php echo $this->lang->line('cargo_tracking') ?></span><i class="fa fa-angle-right"></i>
                        </a>
                        <!--<ul class="treeview-menu">
                            <li><a href="analytic-customer.html">Son Siparişler</a></li>
                        </ul>-->
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>