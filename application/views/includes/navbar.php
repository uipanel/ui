<header class="top-header-area d-flex align-items-center justify-content-between">
    <div class="left-side-content-area d-flex align-items-center">
        <div class="mobile-logo mr-3 mr-sm-4">
            <a href="index.html"><img src="<?php echo base_url("assets") ?>\img\panel\small-logo.png" alt="Mobile Logo"></a>
        </div>
        <div class="ecaps-triggers mr-1 mr-sm-3">
            <div class="menu-collasped" id="menuCollasped">
                <i class='bx bx-menu'></i>
            </div>
            <div class="mobile-menu-open" id="mobileMenuOpen">
                <i class='bx bx-menu'></i>
            </div>
        </div>
        <ul class="left-side-navbar d-flex align-items-center">
            <li class="hide-phone app-search">
                <input type="text" class="form-control" placeholder="Ara...">
                <span class="bx bx-search-alt"></span>
            </li>
        </ul>
    </div>
    <div class="right-side-navbar d-flex align-items-center justify-content-end">
        <div class="right-side-trigger" id="rightSideTrigger">
            <i class='bx bx-menu-alt-right'></i>
        </div>
        <ul class="right-side-content d-flex align-items-center">
            <li class="nav-item dropdown">
                <div class="dropdown d-none d-lg-inline-block ml-1 show">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="bx bx-customize"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <div class="px-lg-2">
                            <div class="row no-gutters">
                                <div class="col">
                                    <a class="dropdown-icon-item" href="#">
                                        <img src="<?php echo base_url("assets") ?>\img\shop-img\18.jpg" alt="image">
                                        <span>Motriza</span>
                                    </a>
                                </div>
                                <div class="col">
                                    <a class="dropdown-icon-item" href="#">
                                        <img src="<?php echo base_url("assets") ?>\img\shop-img\19.jpg" alt="image">
                                        <span>Jisladtd</span>
                                    </a>
                                </div>
                                <div class="col">
                                    <a class="dropdown-icon-item" href="#">
                                        <img src="<?php echo base_url("assets") ?>\img\shop-img\20.jpg" alt="image">
                                        <span>Dribbble</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row no-gutters">
                                <div class="col">
                                    <a class="dropdown-icon-item" href="#">
                                        <img src="<?php echo base_url("assets") ?>\img\shop-img\13.png" alt="image">
                                        <span>GitHub</span>
                                    </a>
                                </div>
                                <div class="col">
                                    <a class="dropdown-icon-item" href="#">
                                        <img src="<?php echo base_url("assets") ?>\img\shop-img\14.png" alt="image">
                                        <span>Google</span>
                                    </a>
                                </div>
                                <div class="col">
                                    <a class="dropdown-icon-item" href="#">
                                        <img src="<?php echo base_url("assets") ?>\img\shop-img\17.jpg" alt="image">
                                        <span>Dribbble</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"><i class='bx bx-bell bx-tada'></i><span class="active-status"></span></button>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="top-notifications-area">
                        <div class="notifications-heading">
                            <div class="heading-title">
                                <h6>Notifications</h6>
                            </div>
                            <span>07 New</span>
                        </div>
                        <div class="notifications-box" id="notificationsBox">
                            <a href="#" class="dropdown-item">
                                <i class='bx bx-shopping-bag'></i>
                                <div>
                                    <span>Your order is placed</span>
                                    <p class="mb-0 font-12">Consectetur adipisicing elit. Ipsa, porro!</p>
                                </div>
                            </a>
                            <a href="#" class="dropdown-item">
                                <img src="<?php echo base_url("assets") ?>\img\member-img\mail-1.jpg" alt="">
                                <div>
                                    <span>Haslina Obeta</span>
                                    <p class="mb-0 font-12">Consectetur adipisicing elit. Ipsa, porro!</p>
                                </div>
                            </a>
                            <a href="#" class="dropdown-item">
                                <i class='bx bx-atom bg-success'></i>
                                <div>
                                    <span>Your order is Dollar</span>
                                    <p class="mb-0 font-12">Consectetur adipisicing elit. Ipsa, porro!</p>
                                </div>
                            </a>
                            <a href="#" class="dropdown-item">
                                <img src="<?php echo base_url("assets") ?>\img\member-img\mail-3.jpg" alt="">
                                <div>
                                    <span>Your order is placed</span>
                                    <p class="mb-0 font-12">Consectetur adipisicing elit. Ipsa, porro!</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"><img src="<?php echo base_url("assets") ?>\img\member-img\contact-2.jpg" alt=""></button>
                <div class="dropdown-menu profile dropdown-menu-right">
                    <div class="user-profile-area">
                        <a href="#" class="dropdown-item"><i class="bx bx-user font-15" aria-hidden="true"></i>My profile</a>
                        <a href="#" class="dropdown-item"><i class="bx bx-wallet font-15" aria-hidden="true"></i>My wallet</a>
                        <a href="#" class="dropdown-item"><i class="bx bx-wrench font-15" aria-hidden="true"></i>settings</a>
                        <a href="#" class="dropdown-item"><i class="bx bx-power-off font-15" aria-hidden="true"></i>Sign-out</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</header>