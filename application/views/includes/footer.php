<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <footer class="footer-area d-flex align-items-center flex-wrap">
                <div class="copywrite-text">
                    <p>UI Panel <?php echo date("Y"); ?> &copy;</p>
                </div>
            </footer>
        </div>
    </div>
</div>