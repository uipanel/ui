<!doctype html>

<head>
    <!-- <head> İçeriği -->
    <?php $this->load->view("includes/head"); ?>
    <!-- ### -->
</head>

<body>
    <!-- Preloader -->
    <?php $this->load->view("includes/preloader"); ?>
    <!-- ### -->

    <div class="ecaps-page-wrapper">
        <!-- Sidebar -->
        <?php $this->load->view("includes/side-menu"); ?>
        <!-- ### -->

        <div class="ecaps-page-content">
            <!-- Navbar -->
            <?php $this->load->view("includes/navbar"); ?>
            <!-- ### -->

            <!-- Ana Kısım -->
            <div class="main-content">
                <!-- İçerik -->
                <?php $this->load->view("{$viewFolder}/{$subViewFolder}/content"); ?>
                <!-- ### -->

                <!-- Footer -->
                <?php $this->load->view("includes/footer"); ?>
                <!-- ### -->
            </div>
            <!-- ### -->
        </div>
    </div>
    <!-- Scriptler -->
    <?php $this->load->view("includes/script"); ?>
    <!-- ### -->
</body>

</html>