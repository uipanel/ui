<div class="container-fluid">
    <div class="row my-2">
        <div class="col-12">
            <div class="card" style="border: none;width: 100%;">
                <div class="row fs_24px p-3 mb-0">
                    <div class="col-6">
                        <label class="mb-0 vertical-center">
                            Ürünler
                        </label>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?php echo base_url("products/add"); ?>" class="btn btn-primary">Ürün Ekle</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php if(empty($items)) {?>
        <div class="col-12 box-grid">
            <div class="card text-center p-4 mb-3" style="width: 100%">
                <div class="row">
                    <div class="col-12">
                        <img src="assets/img/products/no-products.png" width="120px">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row pb-5 justify-content-center text-center">
                        <div class="col-12 fs_22px"></div>
                    </div>
                    <a href="<?php echo base_url("products/add"); ?>" class="btn btn-primary">Ürün Ekle</a>
                </div>
            </div>
        </div>
        <?php }else{ foreach ($items as $products){?>
        <div class="col-12 grid-list">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <img src="assets/img/blog-img/4.jpg" class="card-img">
                    <div class="card-body border-left">
                        <h5 class="card-title"><?php echo $products->title; ?></h5>
                        <p class="card-text"><?php echo $products->description; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php } }?>
    </div>
    <!--<div class="row test">
        <div class="col-lg-3 col-xl-2 mb-3 box-grid">
            <div class="card" style="width: 100%">
                <div class="dropdown popupMenu show text-right">
                    <a class="dropdown-a" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ürünü Sil</a>
                        <a class="dropdown-item" href="#">Ürünü Kaldır</a>
                    </div>
                </div>
                <img src="assets/img/shop-img/2.png" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Ürün Başlık</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="product-price mb-0 mt-0">$20.00</h4>
                        <div class="div">
                            <div class="badge badge-success badge-pill">In stock</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-lg-12">
                            <p class="card-text mt-2">Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir...</p>
                        </div>
                        <div class="col-6 col-lg-12"><a href="#" class="btn btn-primary">Ürünü Görüntüle</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xl-2 mb-3 box-grid">
            <div class="card" style="width: 100%">
                <div class="dropdown popupMenu show text-right">
                    <a class="dropdown-a" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ürünü Sil</a>
                        <a class="dropdown-item" href="#">Ürünü Kaldır</a>
                    </div>
                </div>
                <img src="assets/img/shop-img/2.png" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Ürün Başlık</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="product-price mb-0 mt-0">$20.00</h4>
                        <div class="div">
                            <div class="badge badge-success badge-pill">In stock</div>
                        </div>
                    </div>
                    <p class="card-text mt-2">Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir...
                    </p>
                    <a href="#" class="btn btn-primary">Ürünü Görüntüle</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xl-2 mb-3 box-grid">
            <div class="card" style="width: 100%">
                <div class="dropdown popupMenu show text-right">
                    <a class="dropdown-a" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ürünü Sil</a>
                        <a class="dropdown-item" href="#">Ürünü Kaldır</a>
                    </div>
                </div>
                <img src="assets/img/shop-img/2.png" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Ürün Başlık</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="product-price mb-0 mt-0">$20.00</h4>
                        <div class="div">
                            <div class="badge badge-success badge-pill">In stock</div>
                        </div>
                    </div>
                    <p class="card-text mt-2">Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir...
                    </p>
                    <a href="#" class="btn btn-primary">Ürünü Görüntüle</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xl-2 mb-3 box-grid">
            <div class="card" style="width: 100%">
                <div class="dropdown popupMenu show text-right">
                    <a class="dropdown-a" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ürünü Sil</a>
                        <a class="dropdown-item" href="#">Ürünü Kaldır</a>
                    </div>
                </div>
                <img src="assets/img/shop-img/2.png" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Ürün Başlık</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="product-price mb-0 mt-0">$20.00</h4>
                        <div class="div">
                            <div class="badge badge-success badge-pill">In stock</div>
                        </div>
                    </div>
                    <p class="card-text mt-2">Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir...
                    </p>
                    <a href="#" class="btn btn-primary">Ürünü Görüntüle</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xl-2 mb-3 box-grid">
            <div class="card" style="width: 100%">
                <div class="dropdown popupMenu show text-right">
                    <a class="dropdown-a" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ürünü Sil</a>
                        <a class="dropdown-item" href="#">Ürünü Kaldır</a>
                    </div>
                </div>
                <img src="assets/img/shop-img/2.png" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Ürün Başlık</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="product-price mb-0 mt-0">$20.00</h4>
                        <div class="div">
                            <div class="badge badge-success badge-pill">In stock</div>
                        </div>
                    </div>
                    <p class="card-text mt-2">Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir...
                    </p>
                    <a href="#" class="btn btn-primary">Ürünü Görüntüle</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xl-2 mb-3 box-grid">
            <div class="card" style="width: 100%">
                <div class="dropdown popupMenu show text-right">
                    <a class="dropdown-a" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ürünü Sil</a>
                        <a class="dropdown-item" href="#">Ürünü Kaldır</a>
                    </div>
                </div>
                <img src="assets/img/shop-img/2.png" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Ürün Başlık</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="product-price mb-0 mt-0">$20.00</h4>
                        <div class="div">
                            <div class="badge badge-success badge-pill">In stock</div>
                        </div>
                    </div>
                    <p class="card-text mt-2">Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir...
                    </p>
                    <a href="#" class="btn btn-primary">Ürünü Görüntüle</a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xl-2 mb-3 box-grid">
            <div class="card" style="width: 100%">
                <div class="dropdown popupMenu show text-right">
                    <a class="dropdown-a" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ürünü Sil</a>
                        <a class="dropdown-item" href="#">Ürünü Kaldır</a>
                    </div>
                </div>
                <img src="assets/img/shop-img/2.png" class="card-img-top" alt="">
                <div class="card-body">
                    <h5 class="card-title">Ürün Başlık</h5>
                    <div class="d-flex justify-content-between align-items-center">
                        <h4 class="product-price mb-0 mt-0">$20.00</h4>
                        <div class="div">
                            <div class="badge badge-success badge-pill">In stock</div>
                        </div>
                    </div>
                    <p class="card-text mt-2">Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir...
                    </p>
                    <a href="#" class="btn btn-primary">Ürünü Görüntüle</a>
                </div>
            </div>
        </div>
         Ürün Yoksa ki durum aşağıda
        <div class="col-12 box-grid">
            <div class="card text-center p-4 mb-3" style="width: 100%">
                <div class="row">
                    <div class="col-12">
                        <img src="assets/img/products/no-products.png" width="120px">
                    </div>
                </div>
                <div class="card-body">
                    <div class="row pb-5 justify-content-center text-center">
                        <div class="col-12 fs_22px">Sistemde kayıtlı ürün bulunamadı. İsterseniz hemen ürün eklemeye başlayabilirsiniz.</div>
                    </div>
                    <a href="#" class="btn btn-primary">Ürün Ekle</a>
                </div>
            </div>
        </div>
    </div>-->
</div>