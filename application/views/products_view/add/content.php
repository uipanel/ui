<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-body px-lg-5">
                    <div class="row">
                        <div class="col-12">
                            <form action="<?php echo base_url("products/insert") ?>" method="post"
                                enctype="multipart/form-data">
                                <div class="md-form mt-3">
                                    <input type="text" name="product_name" placeholder="Ürün Adı" class="form-control">
                                </div>
                                <div class="md-form mt-2">
                                    <textarea id="editor1" name="products_description" rows="30"
                                        cols="150"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_price" placeholder="Ürün Fiyatı"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_discount_price"
                                                placeholder="Ürün İndirimli Fiyatı" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="md-form mt-3">
                                    <select name="product_type" class="form-control">
                                        <option disabled selected>
                                            Ürün Tipini Seçiniz!
                                        </option>
                                        <option>
                                            Ürün
                                        </option>
                                        <option>
                                            Sanal
                                        </option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_key_1"
                                                   placeholder="Varyasyon Adı" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_value_1"
                                                   placeholder="Varyasyon Değeri" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_stock_1"
                                                   placeholder="Varyasyon Stok" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_stock_code_1"
                                                   placeholder="Varyasyon Stok Kodu" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_key_2"
                                                placeholder="Varyasyon Adı" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_value_2"
                                                placeholder="Varyasyon Değeri" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_stock_2"
                                                   placeholder="Varyasyon Stok" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_stock_code_2"
                                                   placeholder="Varyasyon Stok Kodu" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_key_3"
                                                   placeholder="Varyasyon Adı" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_value_3"
                                                   placeholder="Varyasyon Değeri" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_stock_3"
                                                   placeholder="Varyasyon Stok" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_variation_stock_code_3"
                                                   placeholder="Varyasyon Stok Kodu" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="md-form mt-3">
                                    <select name="product_category" class="form-control">
                                        <option disabled selected>
                                            Kategori Seçiniz!
                                        </option>
                                        <option>
                                            Elektronik
                                        </option>
                                        <option>
                                            Giyim
                                        </option>
                                        <option>
                                            Aksesuar
                                        </option>
                                        <option>
                                            Ayakkabı
                                        </option>
                                    </select>
                                </div>
                                <div class="md-form mt-3">
                                    <label>Ürün Görselleri</label><input type="file" multiple name="product_images[]"
                                        placeholder="Ürün Görsel" class="form-control">
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_property_key_1"
                                                placeholder="Ürün Nitelik Adı" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_property_value_1"
                                                placeholder="Ürün Nitelik Değeri" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_property_key_2"
                                                placeholder="Ürün Nitelik Adı" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_property_value_2"
                                                placeholder="Ürün Nitelik Değeri" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_property_key_3"
                                                placeholder="Ürün Nitelik Adı" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="md-form mt-3">
                                            <input type="text" name="product_property_value_3"
                                                placeholder="Ürün Nitelik Değeri" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-4 justify-content-center">
                                    <div class="col-12 my-2">
                                        <button type="submit" id="add_products" class="btn btn-primary">Ürün
                                            Ekle</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>