<?php

$lang['main_page'] = "Ana Sayfa";
$lang['products'] = "Ürünler";
$lang['turnover'] = "Ciro";
$lang['daily_turnover'] = "Günlük Ciro";
$lang['weekly_turnover'] = "Haftalık Ciro";
$lang['total_sales_per_day'] = "Günlük Toplam Satış";
$lang['orders'] = "Siparişler";
$lang['call_back'] = "Geri Arama";
$lang['support'] = "Destek";
$lang['cargo_tracking'] = "Kargo Takip";
